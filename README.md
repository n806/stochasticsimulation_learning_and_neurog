Scripts used for the stochastic simulations of the work "Spatial learning promotes adult neurogenesis in delimited regions of the zebrafish pallium".

Instructions: Execute the "prog_apoptosis_promedio.py" script for the stochastic simulations, remember to also download "func_modelo.py".
