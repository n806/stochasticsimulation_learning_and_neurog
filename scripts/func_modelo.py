#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 28 03:15:55 2021

@author: vettocastillo

Functions used for the stochastic simulation used to stimate the populations 
of: rNSCs (reservoir Neural Stem Cells), 
    oNSCs (operative NSCs), and
    n (neurons).
    
Algorithm: Gillespie (see https://en.wikipedia.org/wiki/Gillespie_algorithm).
Note: Neuronal death is not considered on the functions of this script. Instead
of, the neuronal death is included later on the script 
"prog_apoptosis_promedio.py".


Considered cellular processes (Than-Trong et al., Sci. Adv. 2020; 6 : eaaz5424): 
    0) rNSC -> rNSC + oNSC (rate k1 = 0.007 1/day)
    1) oNSC -> oNSC + oNSC (rate k2 = 0.006 1/day)
    2) oNSC -> apoptosis (rate k3 = 0.017 1/day)
    3) oNSC -> oNSC + n (rate k4 = 0.018 1/day)
    4) oNSC -> n + n (rate k5 = 0.004 1/day)
    5) oNSC -> n (rate k6 = 0.013 1/day)
"""
from numpy.random import exponential, uniform
import numpy as np

# Rates [1/day]
k0 = 0.007
k1 = 0.006
k2 = 0.017
k3 = 0.018
k4 = 0.004
k5 = 0.013

def simulacion(learning_factor, d0 = 500, T_END = 30, tasas_factor = 1):
    # this function receives a "learning factor", the initial population of
    # BrdU-labeled cells (d0), the duration of the experiment (T_END) in [days],
    # A parameter to vary the value of all the rates (tasas_factor).
    
    # This function provides the  population of NSCs, neurons, and the 
    # number of divisions.
    Param = [0, 1 , 2]
                                #0: Control situation
                                #1: two learning windows between days 3-7 and 10-14 post BrdU.
                                #2: three learning windows between days 12-16, 19-23 and 26-30 post BrdU
    
    # tasas are the rates.
    tasas = np.array([k0,
                     k1,
                     k2,
                     k3,
                     k4,
                     k5,])
    
    tasas *=tasas_factor        #In some simulations we vary the rate values.
    # the divx (x = 1, 2, 4, 5) values represents the probability'ocurrence of 
    # every single division, and were calculated using an initial population of
    # d0 = 1000 BrdU-labelled cells at steady state (4 rNSC per 1 oNSC).
    div1 = 0.49944455394097936
    div2 = 0.10632760566913332
    div4 = 0.32258425758363046
    div5 = 0.07164358280625684
    
    divisiones = 0
    
    
    Neuronas = []       #Neurons list
    Tempo = []          #Time list
    Operational = []    #oNSC list
    Reservoir = []      #rNSC list
    Rel_divisions = []  #Divisions list
    
    for paradigma in Param:
        n = [int(2*d0*div5 + d0*div4)]
        oNSC = [int(d0*div4 + 2*d0*div2 + d0*div1)]
        rNSC = [int(d0*div1)]
        death_oNSC = [0]
        t = 0.0
        divisiones = 0
        tiempo = [t]
        while (t <= T_END):
            k = tasas
            #Learning paradigm 1
            if (paradigma == 1):
                if (((t >= 3) & (t < 7)) | ((t >= 10) & (t < 14))):
                    k = learning_factor * tasas   
            
            #Learning paradigm 2    
            if (paradigma == 2):
                if (((t >= 12) & (t < 16)) | ((t >= 19) & (t < 23)) | ((t >= 26) & (t < 30))):
                    k = learning_factor * tasas   
            
            h = np.array([rNSC[-1],
                          oNSC[-1], 
                          oNSC[-1], 
                          oNSC[-1], 
                          oNSC[-1],
                          oNSC[-1]])    #Reactive species
            a = h*k
            a_0 = np.sum(a)
            t += exponential(1/a_0)
            w = uniform(0, a_0)
            #rNSC -> rNSC + oNSC
            if (w  <= a[0]):
                rNSC.append(rNSC[-1])
                oNSC.append(oNSC[-1] + 1)
                n.append(n[-1])
                death_oNSC.append(death_oNSC[-1])
                
                divisiones += 1
            #oNSC -> oNSC + oNSC
            if ((w > a[0]) & (w <= np.sum(a[:2]))):
                rNSC.append(rNSC[-1])
                oNSC.append(oNSC[-1] + 1)
                n.append(n[-1])
                death_oNSC.append(death_oNSC[-1])
                
                divisiones += 1
            #oNSC -> apoptosis
            if ((w > np.sum(a[:2])) & (w <= np.sum(a[:3]))):
                rNSC.append(rNSC[-1])
                oNSC.append(oNSC[-1] - 1)
                n.append(n[-1])
                death_oNSC.append(death_oNSC[-1] + 1)
            #oNSC -> oNSC + n
            if ((w > np.sum(a[:3])) & (w <= np.sum(a[:4]))):
                rNSC.append(rNSC[-1])
                oNSC.append(oNSC[-1])
                death_oNSC.append(death_oNSC[-1])
                n.append(n[-1] + 1)
                divisiones += 1
                    
            #oNSC -> n + n
            if ((w > np.sum(a[:4])) & (w <= np.sum(a[:5]))):
                rNSC.append(rNSC[-1])
                oNSC.append(oNSC[-1] - 1)
                death_oNSC.append(death_oNSC[-1])
                n.append(n[-1] + 2)                
                divisiones += 1
                    
                
            #oNSC -> n
            if ((w > np.sum(a[:5])) & (w <= np.sum(a[:6]))):
                rNSC.append(rNSC[-1])
                oNSC.append(oNSC[-1] - 1)
                death_oNSC.append(death_oNSC[-1])
                n.append(n[-1] + 1)
                
            tiempo.append(t)
            
        Neuronas.append(n)
        Tempo.append(tiempo)
        Operational.append(oNSC)
        Reservoir.append(rNSC)
        Rel_divisions.append(1 + (divisiones/d0))
    return Tempo, Neuronas, Rel_divisions, Operational, Reservoir;
    


