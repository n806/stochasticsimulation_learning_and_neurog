#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 28 04:28:47 2021

@author: vettocastillo
Comentario: Neuronal death is incorporated (P_Death), learning-rescue factor, 
and the populations were averaged (vueltas = 20) times.
"""

from func_modelo import simulacion
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
sns.set_context("paper", font_scale=1.5)

learning_factor = [3, 3, 1, 3, 3, 1]    #The learning factor only affects the
                                        # the division rates.

P_Death = 0.55                          #Probability of neuronal death after 15 days
rescate = 0.10                          #Decrease of neuronal death
                                        # (learning-rescue factor)
d0 = 100        #Initial BrdU-labeled cells (in division)
T_END = 30      #days
vueltas = 20    # Number of simulations

tasas_factor = 1

 #Function used to convert continuous to discrete time (in days)
def a_dias(continous_time):             
    tiempos = np.arange(0, T_END+1)
    idx = []
    for t in tiempos:
        for discrete_time in continous_time:
            if discrete_time >= t:
                idx.append(np.where(continous_time == discrete_time)[0][0]) 
                break;
    idx = np.asarray(idx)
    return idx;
Rel_control = []
Rel_2sessions = []
Rel_3sessions = []

Div_ctrl = []
Div_2ss = []
Div_3ss = []

N_ctrl = []
N_2ss = []
N_3ss = []

O_ctrl = []
O_2ss = []
O_3ss = []

R_ctrl = []
R_2ss = []
R_3ss = []


 
for i in range(vueltas):
    Tiempo, Neuronas, divisions, onsc, rnsc = simulacion(learning_factor,
                                                          d0,
                                                          T_END,
                                                          tasas_factor,)
    # Time Arrays
    t_c = np.asarray(Tiempo[0])           
    t_2s = np.asarray(Tiempo[1])     
    t_3s = np.asarray(Tiempo[2])
    
    # Neurons
    n_c = np.asarray(Neuronas[0])
    n_2 = np.asarray(Neuronas[1])
    n_3 = np.asarray(Neuronas[2])
    
    N_ctrl.append(n_c[a_dias(t_c)])
    N_2ss.append(n_2[a_dias(t_2s)])
    N_3ss.append(n_3[a_dias(t_3s)])
    
    # oNSCs
    o_c = np.asarray(onsc[0])
    o_2 = np.asarray(onsc[1])
    o_3 = np.asarray(onsc[2])
    
    O_ctrl.append(o_c[a_dias(t_c)])
    O_2ss.append(o_2[a_dias(t_2s)])
    O_3ss.append(o_3[a_dias(t_3s)])
    
    # rNSCs
    r_c = np.asarray(rnsc[0])
    r_2 = np.asarray(rnsc[1])
    r_3 = np.asarray(rnsc[2])
    
    R_ctrl.append(r_c[a_dias(t_c)])
    R_2ss.append(r_2[a_dias(t_2s)])
    R_3ss.append(r_3[a_dias(t_3s)])
    
    #Relative number of neurons (over total BrdU-labeled cells)
    r_n_c = np.asarray(Neuronas[0])/Neuronas[0][-1]
    r_n_2s = np.asarray(Neuronas[1])/Neuronas[0][-1]
    r_n_3s = np.asarray(Neuronas[2])/Neuronas[0][-1]
    
    Rel_control.append(r_n_c[a_dias(t_c)])
    Rel_2sessions.append(r_n_2s[a_dias(t_2s)])
    Rel_3sessions.append(r_n_3s[a_dias(t_3s)])
    
    #Number of divisions
    Div_ctrl.append(divisions[0])
    Div_2ss.append(divisions[1])
    Div_3ss.append(divisions[2])


time = np.arange(0, T_END+1)
deathN_ctrl = np.zeros(shape=(T_END+1))
deathN_2ss = np.zeros(shape=(T_END+1))
deathN_3ss = np.zeros(shape=(T_END+1))
t_death = 15        # The neurons could die after t_death days

# Neurons
N_ctrl = np.asarray(N_ctrl)
N_2ss = np.asarray(N_2ss)
N_3ss = np.asarray(N_3ss)

N_mean_ctrl = np.mean(N_ctrl, axis = 0)
N_mean_2ss = np.mean(N_2ss, axis = 0)
N_mean_3ss = np.mean(N_3ss, axis = 0)

N_mean_ctrl = np.hstack((0, N_mean_ctrl))
N_mean_2ss = np.hstack((0, N_mean_2ss))
N_mean_3ss = np.hstack((0, N_mean_3ss))


#Here we include the neuronal death
for t in range(T_END+1 - t_death):
    deathN_ctrl[t + t_death] = P_Death*(N_mean_ctrl[t+1] - N_mean_ctrl[t])
    deathN_2ss[t + t_death] = P_Death*(N_mean_2ss[t+1] - N_mean_2ss[t])
    if (((t+t_death >= 15) & (t+t_death <= 16)) | ((t+t_death >= 19) & (t+t_death <= 23)) | ((t+t_death >= 26) & (t+t_death <= 30))):
        deathN_3ss[t + t_death] = P_Death*rescate*(N_mean_3ss[t+1] - N_mean_3ss[t])
    else:
        deathN_3ss[t + t_death] = P_Death*(N_mean_3ss[t+1] - N_mean_3ss[t])

deathN_ctrl = np.cumsum(deathN_ctrl)
deathN_2ss = np.cumsum(deathN_2ss)
deathN_3ss = np.cumsum(deathN_3ss)

N_mean_ctrl = N_mean_ctrl[1:]
N_mean_2ss = N_mean_2ss[1:]
N_mean_3ss = N_mean_3ss[1:]    

    
#Neurons        
N_mean_ctrl -= deathN_ctrl
N_mean_2ss -= deathN_2ss
N_mean_3ss -= deathN_3ss

N_std_ctrl = np.std(N_ctrl, axis = 0)
N_std_2ss = np.std(N_2ss, axis = 0)
N_std_3ss = np.std(N_3ss, axis = 0)

# oNSC
O_ctrl = np.asarray(O_ctrl)
O_2ss = np.asarray(O_2ss)
O_3ss = np.asarray(O_3ss)

O_mean_ctrl = np.mean(O_ctrl, axis = 0)
O_mean_2ss = np.mean(O_2ss, axis = 0)
O_mean_3ss = np.mean(O_3ss, axis = 0)

O_std_ctrl = np.std(O_ctrl, axis = 0)
O_std_2ss = np.std(O_2ss, axis = 0)
O_std_3ss = np.std(O_3ss, axis = 0)

# rNSC
R_ctrl = np.asarray(R_ctrl)
R_2ss = np.asarray(R_2ss)
R_3ss = np.asarray(R_3ss)

R_mean_ctrl = np.mean(R_ctrl, axis = 0)
R_mean_2ss = np.mean(R_2ss, axis = 0)
R_mean_3ss = np.mean(R_3ss, axis = 0)

R_std_ctrl = np.std(R_ctrl, axis = 0)
R_std_2ss = np.std(R_2ss, axis = 0)
R_std_3ss = np.std(R_3ss, axis = 0)

# relative number of neurons (trained/control)
Rel_control = np.asarray(Rel_control)
Rel_2sessions = np.asarray(Rel_2sessions)
Rel_3sessions = np.asarray(Rel_3sessions)

Rel_mean_control = np.mean(Rel_control, axis = 0)
Rel_mean_2ss = np.mean(Rel_2sessions, axis = 0)
Rel_mean_3ss = np.mean(Rel_3sessions, axis = 0)

Rel_std_control = np.std(Rel_control, axis = 0)
Rel_std_2ss = np.std(Rel_2sessions, axis = 0)
Rel_std_3ss = np.std(Rel_3sessions, axis = 0)

'''
Graphs zone
'''

ax = plt.axes()
plt.plot(time, N_mean_ctrl, '-o', label = 'Control', color = 'black')
plt.fill_between(time, N_mean_ctrl - N_std_ctrl, N_mean_ctrl + N_std_ctrl, color = 'black', alpha = 0.2)
plt.plot(time, N_mean_2ss, '-^', label = '3-14 dpi', color = 'red')
plt.fill_between(time, N_mean_2ss - N_std_2ss, N_mean_2ss + N_std_2ss, color = 'red', alpha = 0.2)
plt.plot(time, N_mean_3ss, '-o', label = '12-30 dpi', color = 'blue')
plt.fill_between(time, N_mean_3ss - N_std_3ss, N_mean_3ss + N_std_3ss, color = 'blue', alpha = 0.2)
plt.legend(loc = 'best')
plt.xlabel('dpi')
plt.ylabel('Population of Neurons')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.tight_layout()
plt.show()



